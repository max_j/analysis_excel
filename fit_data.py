import pandas as pd
import numpy as np
from scipy.optimize import curve_fit
import warnings
import matplotlib.pyplot as plt

# log2 funkcja
def log2(x, a, b):
    return a * np.log2(x) + b


# funkcja znajdująca optymalne parametry
def find_params(func, x, y, tab_name):

    # znajdowanie parametrów
    parameters, covariance = curve_fit(func, x, y)

    # znajdź sekwencje zmiennej x pomiędzy najmiejszym a największym punktem
    x_line = np.arange(min(x), max(x), 1)

    # znajdź predykcje dla tych punktów z x
    y_line = func(x_line, *parameters)

    # predykcja wszystkich punktów x
    y_pred = func(x, *parameters)

    # sum of squared errors
    sse = sum_squared_errors(y, y_pred)

    # eksport wykresów
    plt.rcParams["figure.figsize"] = 12, 8
    plt.scatter(x, y)
    plt.plot(x_line, y_line, '--', color='red')
    title = tab_name + ' | ' + func.__name__

    plt.ylabel('1+')
    plt.xlabel(
        'GRP\n Sum squared errors: ' + str(sse) + '\n' + 'Parameters: a:' + str(parameters[0]) + '  |  b: ' + str(
            parameters[1]))

    plt.title(title)
    plt.savefig('graphs/' + tab_name)
    plt.show()


    # print('Sum squared errors: ', sse)
    # print('Parameters: ', parameters)

    return parameters, sse


def process_row(row, func):
    results_row = dict()

    tab = pd.read_excel(r'data/data.xlsx', sheet_name = row['Tab name'])

    tab = tab[1:]
    x = tab.GRP
    y = tab['1+']

    parameters, sse = find_params(func, x, y, 'Tab ' + str(row['Id']))

    #     sse_per_tab[f.__name__] = sse
    results_row['Id'] = int(row['Id'])
    results_row['Name'] = row['Name']
    results_row['a'] = parameters[0]
    results_row['b'] = parameters[1]
    results_row['SSE'] = sse

    return results_row


# funcja wyliczająca sumę błędów kwadratowych
def sum_squared_errors(y_true, y_pred):
    sse = np.sum((y_true - y_pred) ** 2)
    return sse

# funcja main
def main():

    #wczytanie arkusza META z pliku excel
    meta = pd.read_excel(r'data/data.xlsx', sheet_name = 'META')


    meta.columns = meta.iloc[0]
    meta = meta[['Id', 'Tab name', 'Name']]
    meta = meta.drop(0)

    df = pd.DataFrame([], columns=['Id', 'Name', 'a', 'b', 'SSE'])
    for row in range(len(meta)):
        ret_dict = process_row(meta.iloc[row], log2)
        df_temp = pd.DataFrame(ret_dict, index=[row])
        df = df.append(df_temp)

    # df = df.set_index('Id')

    print(df.to_string(index=False))

# if __name__ == "__main__":
#     main()

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    main()