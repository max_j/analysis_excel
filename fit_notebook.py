# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.11.3
#   kernelspec:
#     display_name: nlp
#     language: python
#     name: nlp
# ---

import pandas as pd

from scipy.optimize import curve_fit
import numpy as np

meta = pd.read_excel(r'data/data.xlsx', sheet_name='META')

meta

meta.columns = meta.iloc[0]

meta = meta[['Id', 'Tab name', 'Name']]

meta = meta.drop(0)



meta

# +
df = pd.DataFrame([], columns=['Id', 'Name', 'a', 'b', 'SSE'])
for row in range(len(meta)):
    ret_dict = return_from_row(meta.iloc[row], log2)
    df_temp = pd.DataFrame(ret_dict, index=[row])
    df = df.append(df_temp)
    
    
    

# -

df

df = df.set_index('Id')

df









meta.iloc[0]



row = meta.iloc[0]


# +
def return_from_row(row, func):
    results_row = dict()
    
    tab = pd.read_excel(r'data/data.xlsx', sheet_name=row['Tab name'])
        
    
    tab = tab[1:]
    x = tab.GRP
    y = tab['1+']
    
    parameters, sse = find_params(func, x, y)
    
#     sse_per_tab[f.__name__] = sse
    results_row['Id'] = int(row['Id'])
    results_row['Name'] = row['Name']
    results_row['a'] = parameters[0]
    results_row['b'] = parameters[1]
    results_row['SSE'] = sse
    
    return results_row
    
# -



def sum_squared_errors(y_true, y_pred):
     sse = np.sum((y_true-y_pred)**2)
     return sse


# +
return_from_row(meta.iloc[0], log2)


# -

# log2 function
def log2(x, a, b):
	return a * np.log2(x) + b



def find_params(func, x, y):
    
    # znajdowanie parametrow
    parameters, covariance = curve_fit(func, x, y)
    
    # znajdz sekwencje zmiennej x pomiedzy manjmiejszym a najwiekszym punktem
    x_line = np.arange(min(x), max(x), 1)
    
    # znajdź predykcje dla tych puktów z x
    y_line = func(x_line, *parameters)
    
    # predykcja wszystkich punktów x
    y_pred = func(x, *parameters)
    
    # sum of squared errors
    sse = sum_squared_errors(y, y_pred)
    
    
    print('Sum squared errors: ', sse)
    print('Parameters: ', parameters)
    
    return parameters, sse


